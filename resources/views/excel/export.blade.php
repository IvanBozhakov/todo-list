<!DOCTYPE html>
<html>
<head>
	<title>Exports</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
</head>
<body>
	<table>
		<tr>
			<th>#</th>
			<th>Date</th>
			<th>Title</th>
			<th>Description</th>
		</tr>
		@foreach($notes as $key => $note)
		<tr>
			<td>{{$key+1}}</td>
			<td>{{$note->date}}</td>
			<td>{{$note->title}}</td>
			<td>{{$note->description}}</td>
		</tr>
		@endforeach
	</table>
</body>
</html>