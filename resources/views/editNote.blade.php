@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel">
                <h3>Edit Note</h3>
                @if($errors->any())
                <div class="alert alert-danger">
                  {{$errors->first()}}
                </div>
                @endif
                @if(session('message'))
                     <div class="alert alert-success">
                      {{ session('message') }}
                    </div>
                @endif 
                <form action="{{asset('note/post')}}" method="POST">
                     {{ csrf_field() }}
                     <label>Title</label>
                    <input required="required" type="text" name="title"  class="form-control" value="{{$note->title}}" />
                    <input type="hidden" required="required" name="note" value="{{$note->id}}">
                    <p>
                        <label>Status</label>
                        <select name="status" class="form-control">
                            <option value="0" @if($note->status == 0) selected @endif >Open</option>
                            <option value="1" @if($note->status == 1) selected @endif >Close</option>
                        </select>
                    </p>
                    <p>
                        <label>Description</label>
                        <textarea cols="60" rol="6" placeholder="Description here..." class="form-control" name="description">{{$note->description}}</textarea>
                    </p>
                   
                    <p>
                        <br>
                        <button class="btn btn-primary" type="submit">Update</button>
                    </p>
                    
                </form>


            </div>
        
    </div>
</div>
@endsection
