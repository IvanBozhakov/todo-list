@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel">
                <h3>Add new Note</h3>
                @if($errors->any())
                <div class="alert alert-danger">
                  {{$errors->first()}}
                </div>
                @endif
                @if(session('message'))
                     <div class="alert alert-success">
                      {{ session('message') }}
                    </div>
                @endif 
                <form action="{{asset('note/create')}}" method="POST">
                     {{ csrf_field() }}
                    <input required="required" type="text" name="title"  class="form-control" placeholder="Enter title" value="{{old('title')}}" />
                    <input type="hidden" required="required" name="notebook" value="{{$notebook}}">
                    <p>
                        <br>
                         <textarea cols="60" rol="6" placeholder="Description here..." class="form-control" name="description">{{old('description')}}</textarea>
                    </p>
                   
                    <p>
                        <br>
                        <button class="btn btn-primary" type="submit">Create</button>
                    </p>
                    
                </form>


            </div>
        
    </div>
</div>
@endsection
