@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div  class="col-md-10 col-md-offset-1">
          <div class="panel panel-default">
            <div class="panel-heading">Filter by</div>
            <div class="panel-body"> 
              <form action="{{asset('notebooks')}}" class="form-inline my-2 my-lg-0" style="display: inline;position: relative;top:10px;">
                  <select name="status" class="form-control mr-sm-2">
                      <option value > -- All -- </option>
                      <option value="0" @if(isset($filter) && isset($filter['status']) && $filter['status'] == "0") selected @endif >Open</option>
                      <option value="1" @if(isset($filter) && isset($filter['status']) && $filter['status'] == "1") selected @endif>Close</option>
                  </select>
                  <input type="date" name="date" class="form-control mr-sm-2" @if(isset($filter) &&  isset($filter['date'])) 
                  value="{{$filter['date']}}" @endif/>
                  <button class="btn btn-warning my-2 my-sm-0" type="submit">Filter</button>
                </form>
            </div>
          </div>
        </div>
        <div class="col-md-10 col-md-offset-1">
            <div class="panel">
                @if($errors->any())
                <div class="alert alert-danger">
                  {{$errors->first()}}
                </div>
                @endif
                @if(session('message'))
                     <div class="alert alert-success">
                      {{ session('message') }}
                    </div>
                @endif 
                <h3>Add new Notebook</h3>
                <form action="{{asset('notebooks/create')}}" method="POST">
                     {{ csrf_field() }}
                    <input type="text" name="title"  class="form-control" placeholder="Enter title" />
                    <p>
                        <br>
                        <button class="btn btn-success" type="submit">Add</button>
                    </p>
                    
                </form>


            </div>
        </div>
        @foreach($notebooks as $notebook)
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        {{$notebook->title}}
                        <a href="{{asset('note/'.$notebook->id)}}">+ New Note</a>
                        <span class="{{config('notebook')[$notebook->status]['class']}}">{{config('notebook')[$notebook->status]['title']}}</span>
                       
                        @if(config('notebook')[$notebook->status]['title'] != 'deleted')
                        <form action="{{asset('notebooks/delete')}}" id="notebook_{{$notebook->id}}" method="POST" style="float:right;">
                            {{ csrf_field() }}
                            <input type="hidden" name="notebook" value="{{$notebook->id}}" />
                            <div type="butt" class="btn btn-danger"  
                            onclick="notebook.delete({{$notebook->id}})" style="font-size: 10px;">Delete</div>
                        </form>
                        @endif
                         <form action="{{asset('excel/export')}}"  method="GET" style="float:right;">
                            <input type="hidden" name="notebook" value="{{$notebook->id}}" />
                            <input type="hidden" name="status" @if(isset($filter['status'])) value="{{$filter['status']}}" @endif/>
                            <input type="hidden" name="date" @if(isset($filter['status'])) value="{{$filter['date']}}" @endif />
                            <button type="butt" class="btn btn-primary"   style="font-size: 10px;" type="submit">Excel</button>
                        </form>
                    </div>

                    <div class="panel-body"> 
                       @foreach($notebook->notes as $key => $note) 
                       <div>
                            <form action="" method="post" style="display: inline;">
                               <label>#{{$key+1}}</label>
                               <label><a href="{{asset('note/update/'.$note->id)}}">{{$note->title}}</a></label>
                               <label>Date: {{$note->date}}</label>
                               @if($note->status == 0)
                               <label class="label label-success">open</label>
                               @else
                               <label class="label label-danger">close</label>
                               @endif
                            </form>
                            <form action="{{asset('note/delete')}}" id="note_{{$note->id}}" method="POST" style="float:right;display: inline;">
                            {{ csrf_field() }}
                            <input type="hidden" name="note" value="{{$note->id}}" />
                            <span type="butt" class="btn btn-danger" 
                             onclick="note.delete({{$note->id}})" style="font-size: 10px;">Delete</span>
                        </form>
                         <div>
                          <label>Description:</label>
                          <p>{{$note->description}}</p>
                        </div>
                        <hr>
                       </div>
                       @endforeach
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
@endsection
