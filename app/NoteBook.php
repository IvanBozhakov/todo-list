<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NoteBook extends Model
{
    protected $table = "notebooks";
    public $timestamps = false;


    public function user()
    {
    	return $this->hasOne("App\User",'id','user_id');
    }

    public function notes()
    {
    	return $this->hasMany('App\Note','notebook_id','id');
    }
}
