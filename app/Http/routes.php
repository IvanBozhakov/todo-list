<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::auth();

Route::group(['prefix' => 'notebooks'],function(){
	Route::get('/', 'NoteBookController@index');
	Route::post('/create', 'NoteBookController@create');
	Route::post('/delete', 'NoteBookController@delete');
});


Route::group(['prefix' => 'note'],function(){
	Route::get('/{notebook}', 'NoteController@create');
	Route::post('/create', 'NoteController@store');
	Route::get('/update/{note_id}', 'NoteController@update');
	Route::post('/post', 'NoteController@post');
	Route::post('/delete','NoteController@delete');
	
});

Route::group(['prefix' => 'admin'],function(){
	Route::get('/', 'AdminController@index');
});

Route::group(['prefix' => 'excel'],function(){
	Route::get('/export', 'ExcelController@export');
});

