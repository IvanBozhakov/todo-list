<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Note;
use App\NoteBook;
use App\Contracts\iExcel;
use \App\Exceptions\NoteException;
class ExcelController extends Controller
{
    public function __construct()
    {
    	$this->middleware('auth');
    }

    public function export(Request $request, NoteBook $notebook,  iExcel $excel)
    {

        $notebooks = $notebook::with(['notes'=>function($query) use ($request){
           
            if($request->has('status')){
                 $query->where('status',$request->get('status'));
            }

            if($request->has('date')){

                $date = date_create($request->get('date'));
                $formated = date_format($date, 'Y-m-d');
                $query->where('date', '=', $formated);

            }

           
        }])->where('id',$request->get('notebook'))->where('user_id',\Auth::user()->id)->orderBy('id','desc')->first();
    	try{
            $excel->export($notebooks);
        }catch(NoteException $ex){
           return \Redirect::to('notebooks')->withErrors([$ex->getMessage()]);
        }
    	

    }
}
