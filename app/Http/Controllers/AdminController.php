<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\NoteBook;

class AdminController extends Controller
{
   	public function __construct()
    {
        $this->middleware(['auth','admin']);
  
    }

    public function index(NoteBook $notebook, Request $request){

		$notebooks = $notebook::with(['notes'=>function($query) use ($request){
           
            if($request->has('status')){
                 $query->where('status',$request->get('status'));
            }

            if($request->has('date')){

                $date = date_create($request->get('date'));
                $formated = date_format($date, 'Y-m-d');
                $query->where('date', '=', $formated);

            }

           
        }])->orderBy('id','desc')->get();

    	return view('admin.panel')->with(['notebooks'=>$notebooks,'filter'=>$request->all()]);
    }
}
