<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\NoteBook;
class NoteBookController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
  
    }

    public function index(NoteBook $notebook, Request $request){

		$notebooks = $notebook::with(['notes'=>function($query) use ($request){
           
            if($request->has('status')){
                 $query->where('status',$request->get('status'));
            }

            if($request->has('date')){

                $date = date_create($request->get('date'));
                $formated = date_format($date, 'Y-m-d');
                $query->where('date', '=', $formated);

            }

           
        }])->where('user_id',\Auth::user()->id)->orderBy('id','desc')->get();

    	return view('notebooks')->with(['notebooks'=>$notebooks,'filter'=>$request->all()]);
    }


    public function create(Request $request, NoteBook $notebook){
    	
    	$validator = \Validator::make(
		    array('title' => $request->get('title')),
		    array('title' => array('required', 'max:200'))
		);
    	
    	if($validator->fails()){
    		 return \Redirect::to('notebooks')->withErrors($validator);
    	}

    	$notebook->title = $request->get('title');
    	$notebook->user_id = \Auth::user()->id;
    	$notebook->save();

    	return \Redirect::to('notebooks')->with('message', 'Notebook create success!');
    }

    public function delete(Request $request, Notebook $notebook)
    {
    	if(\Auth::user()->isAdmin()){
    		$notebook = $notebook->where('id',$request->get('notebook'))->first();
    	}else{
    		$notebook = $notebook->where('user_id',\Auth::user()->id)->where('id',$request->get('notebook'))->first();
    	}
    	

    	if($notebook && !\Auth::user()->isAdmin()){

    		$notebook->status = 1; //status deleted
    		$notebook->save();

    		return \Redirect::to('notebooks')->with('message', 'Notebook was mark as deteted.Wait approve from administrator!');
    	}else if($notebook && \Auth::user()->isAdmin()){
    		$notebook->delete();
    		return \Redirect::to('notebooks')->with('message', 'Notebook was delete!');
    	}

    	return \Redirect::to('notebooks');
    }

    public function archive(Request $request, Notebook $notebook)
    {
    	# todo
    }
}
