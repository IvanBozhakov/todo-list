<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Note;
use App\NoteBook;

class NoteController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
  
    }

    public function create($notebook){
    	return view('addNote')->with('notebook',$notebook);
    }

    public function update($note_id, Note $note){
    	$note = $note->find($note_id);
    	return view('editNote')->with('note',$note);
    }

    public function store(Request $request, Note $note,NoteBook $notebook)
    {

    	$validator = $this->validateNote($request);

    	if($validator->fails()){
    		 return redirect()->back()->withErrors($validator)->withInput($request->all());;
    	}

    	$notebook = $notebook->where('user_id',\Auth::user()->id)->where('id',$request->get('notebook'))->first();

    	if($notebook == null){
    		return redirect('notebooks');
    	}

    	$note->title = $request->get('title');
    	$note->description = $request->get('description');
    	$note->notebook_id = $notebook->id;
    	$note->date = date('Y-m-d');
    	$note->save();

    	return redirect('notebooks')->with('message','Note added successfuly!');
    }

    public function post(Request $request, Note $note)
    {

    	$validator = $this->validateNote($request);

    	if($validator->fails()){
    		 return redirect()->back()->withErrors($validator);
    	}

    	$note = $note->find($request->get('note'));
    	if($note == null){
    		return redirect()->back()->withErrors(['Not found note']);
    	}

    	$note->title = $request->get('title');
    	$note->description = $request->get('description');
    	$note->status = $request->get('status');
    	$note->save();

    	return redirect()->back()->with('message','Note update successfuly!');
    }

    private function validateNote(Request $request){
    	$validator = \Validator::make(
		    array(
		    	'title' => $request->get('title'),
		    	'description' => $request->get('description')
		    ),
		    array(
		    	'title' => array('required', 'max:200'),
		    	'description' => array('required', 'min:20')
		    )
		);
		return $validator;
    }

    public function delete(Request $request, Note $note)
    {
    	$note = $note->find($request->get('note'));
    	
    	if($note->notebook->user_id == \Auth::user()->id){
    		$note->delete();
    		redirect()->back()->with('message', 'Note was delete successfuly!');

    	}
    	return redirect()->back();
    }

}
