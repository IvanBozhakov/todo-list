<?php
namespace App\Contracts;
use App\NoteBook;
interface iExcel{
	public function export(NoteBook $notebook);
}