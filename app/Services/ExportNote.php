<?php 
namespace App\Services;
use App\Contracts\iExcel;
use App\NoteBook;

class ExportNote implements iExcel
{

	public function export(NoteBook $notebook)
	{

		\Excel::create('notes', function($excel) use ($notebook){

		    $excel->sheet('My notes', function($sheet) use ($notebook){

		    	if($notebook->notes->count() == 0){
		    		throw new \App\Exceptions\NoteException("Cannot export empty notebooks");
		    	}
		        $sheet->loadView('excel.export')->with('notes',$notebook->notes);

		    });

		})->export('xls');
	}


}