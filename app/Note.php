<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    protected $table = "notes";
    public $timestamps = false;

    public function notebook(){
    	return $this->hasOne('App\NoteBook','id','notebook_id');
    }

}
