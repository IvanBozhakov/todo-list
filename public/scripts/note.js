var Note = (function(){
	function Note(){}

	Note.prototype.delete = function(id){
		var conf = confirm("Are you sure you want to delete this note?");

		if(conf){
			$('#note_'+id).submit();
		}
	}

	return Note;
})();

var note = new Note();