var NoteBook = (function(){
	function NoteBook(){}

	NoteBook.prototype.delete = function(id){
		var conf = confirm("Are you sure you want to delete this notebook?");

		if(conf){
			$('#notebook_'+id).submit();
		}
	}

	return NoteBook;
})();

var notebook = new NoteBook();