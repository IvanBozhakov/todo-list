<?php 
return [
	0=> ['title'=>'open', 'class' => 'label label-success'],
	1=> ['title'=>'deleted', 'class' => 'label label-danger'],
	2=> ['title'=>'archive', 'class' => 'label label-default']	
];